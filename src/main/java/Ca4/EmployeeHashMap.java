package Ca4;
//q3
/*
Implement Question 1 (Employee security system) by encapsulating the Collection in a class
called Employee [Type of Collection you have chosen] e.g. EmployeeList. Client programs
should never be able to get direct access to entries in your implementation.
Refer to the sample code in Moodle showing encapsulation of a Student class.
This will be marked using a grading system. You must be able to clearly demonstrate your
understanding of your solution.*/
import java.util.HashMap;
import Ca4.Enums;

/**
 *
 * @author Aidan BLigh D00219125
 */
public class EmployeeHashMap {

    private HashMap<String,Employee> hashMap = new HashMap<String, Employee>();

    /**
     *Constructor for EmployyeHashmap Class
     */
    public EmployeeHashMap() {
        this.hashMap = generateMap();
    }

    //the employee's id is souted on creation as there is no way to search for them in main without breaking the rules of encapsulation
    //this wont be the case in the actual application as new id's wont be generated every time the system is booted
    //this is just so I don't have to break the rules of encapsulation
    private HashMap generateMap() {
        //HashMap<String, Employee> hashMap = new HashMap<String, Employee>();
        Employee test = new Employee("Harvey", "Photo of Harvey", Enums.Departments.MANAGER);
        hashMap.put(test.getId(), test);
        System.out.println(test.getId());
        Employee test2 = new Employee("Joey", "Photo of Joey", Enums.Departments.COOK);
        hashMap.put(test2.getId(), test2);
        System.out.println(test2.getId());
        Employee test3 = new Employee("Mick", "Photo of Mick", Enums.Departments.NETWORK_ADMIN);
        hashMap.put(test3.getId(), test3);
        System.out.println(test3.getId());
        Employee test4 = new Employee("Mike", "Photo of Mike", Enums.Departments.RECEPTIONIST);
        hashMap.put(test4.getId(), test4);
        System.out.println(test4.getId());
        Employee test5 = new Employee("Marv", "Photo of Marv", Enums.Departments.CUSTODIAN);
        hashMap.put(test5.getId(), test5);
        System.out.println(test5.getId());
        Employee test6 = new Employee("Mark", "Photo of Mark", Enums.Departments.SALESMAN);
        hashMap.put(test6.getId(), test6);
        System.out.println(test6.getId());
        Employee test7 = new Employee("Monty", "Photo of Monty", Enums.Departments.SALESMAN);
        hashMap.put(test7.getId(), test7);
        System.out.println(test7.getId());
        return hashMap;
    }

    /**
     *Adds an employee to the hashmap
     * @param emp
     */
    public void addToEmployeeMap(Employee emp)
    {
        hashMap.put(emp.getId(), emp);
    }

    /**
     *
     * @return employee hashmap
     */
    public HashMap<String, Employee> getHashMap() {
        return hashMap;
    }
    //My UUID solution doesn't work well for encapsulation as they are randomly generated each time the program is run.
    // This is to just to get it working in Main but I
//    public String getEmployeeID(){
//
//    }
}