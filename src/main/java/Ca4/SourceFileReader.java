//package Ca4;
//
//import java.io.File;
//import java.io.IOException;
//import  java.util.Scanner;
//import  java.util.Arrays;
//
////https://stackoverflow.com/questions/9858118/whats-the-proper-way-to-compare-a-string-to-an-enum-value
//public class SourceFileReader {
//    public static void main(String[] args) throws Exception {
//        readWordByLine("PetRegistryMenu.java");
//    }
//    /*I know this is a ridiculous way of doing things, I started while i was sick and apparently I don't think diminishing returns affect me.
//     I can do the .usedelimiter function with the regex but I don't know why I would need to.
//     For the sake of your sanity don't try to figure out what I was thinking
//     */
//    public static void readLineByLine(String file)
//    {
//        int intCount = 0;
//        int intIndex[];
//        int doubleCount = 0;
//        int doubleIndex[];
//        int charCount = 0;
//        int charIndex[];
//        int floatCount = 0;
//        int floatIndex[];
//        int bitCount = 0;
//        int bitIndex[];
//        int booleanCount = 0;
//        int booleanIndex[];
//        int byteCount = 0;
//        int byteIndex[];
//        int shortCount = 0;
//        int shortIndex[];
//        int stringCount = 0;
//        int stringIndex[];
//        int ifCount = 0;
//        int ifIndex[];
//        int elseCount = 0;
//        int elseIndex[];
//        int switchCount = 0;
//        int switchIndex[];
//        int forCount = 0;
//        int forIndex[];
//        int whileCount = 0;
//        int whileIndex[];
//        int doCount = 0;
//        int doIndex[];
//        int tryCount = 0;
//        int tryIndex[];
//        int catchCount = 0;
//        int catchIndex[];
//        int publicCount = 0;
//        int publicIndex[];
//        int staticCount = 0;
//        int staticIndex[];
//        int voidCount = 0;
//        int voidIndex[];
//        int mainCount = 0;
//        int mainIndex[];
//        int privateCount = 0;
//        int privateIndex[];
//        int abstractCount = 0;
//        int abstractIndex[];
//        int extendsCount = 0;
//        int extendsIndex[];
//        int throwsCount = 0;
//        int throwsIndex[];
//        int implementsCount = 0;
//        int implementsIndex[];
//        int thisCount = 0;
//        int thisIndex[];
//        int finalCount = 0;
//        int finalIndex[];
//
//        int lineCount = 0;
//        try {
//            Scanner sc = new Scanner(new File(file));
//            while (sc.hasNextLine()) {
//                String word = sc.next();
//                if(enumContainsValue(word))
//                {
//                    Identifiers id = Identifiers.valueOf(word);
//                    switch (id)
//                    {
//                        case INT:
//                            intCount++;
//                            intIndex += lineCount;
//                            break;
//                        case DOUBLE_COUNT:
//                            doubleCount++;
//                            doubleIndex += lineCount;
//                            break;
//                        case CHAR_COUNT:
//                            charCount++;
//                            charIndex += lineCount;
//                            break;
//                        case FLOAT_COUNT:
//                            floatCount++;
//                            floatIndex += lineCount;
//                            break;
//                        case BIT_COUNT:
//                            bitCount++;
//                            bitIndex += lineCount;
//                            break;
//                        case BOOLEAN_COUNT:
//                            booleanCount++;
//                            booleanIndex+= lineCount;
//                            break;
//                        case BYTE_COUNT:
//                            byteCount++;
//                            byteIndex+= lineCount;
//                            break;
//                        case SHORT_COUNT:
//                            shortCount++;
//                            shortIndex+= lineCount;
//                            break;
//                        case STRING_COUNT:
//                            stringCount++;
//                            stringIndex+= lineCount;
//                            break;
//                        case IF_COUNT:
//                            ifCount++;
//                            ifIndex+= lineCount;
//                            break;
//                        case ELSE_COUNT:
//                            elseCount++;
//                            elseIndex+= lineCount;
//                            break;
//                        case SWITCH_COUNT:
//                            switchCount++;
//                            switchIndex+= lineCount;
//                            break;
//                        case FOR_COUNT:
//                            forCount++;
//                            forIndex+= lineCount;
//                            break;
//                        case WHILE_COUNT:
//                            whileCount++;
//                            whileIndex+= lineCount;
//                            break;
//                        case DO_COUNT:
//                            doCount++;
//                            doIndex+= lineCount;
//                            break;
//                        case TRY_COUNT:
//                            tryCount++;
//                            tryIndex+= lineCount;
//                            break;
//                        case CATCH_COUNT:
//                            catchCount++;
//                            catchIndex+= lineCount;
//                            break;
//                        case PUBLIC_COUNT:
//                            publicCount++;
//                            publicIndex+= lineCount;
//                            break;
//                        case STATIC_COUNT:
//                            staticCount++;
//                            staticIndex+= lineCount;
//                            break;
//                        case VOID_COUNT:
//                            voidCount++;
//                            voidIndex+= lineCount;
//                            break;
//                        case MAIN_COUNT:
//                            mainCount++;
//                            mainIndex+= lineCount;
//                            break;
//                        case PRIVATE_COUNT:
//                            privateCount++;
//                            privateIndex+= lineCount;
//                            break;
//                        case ABSTRACT_COUNT:
//                            abstractCount++;
//                            abstractIndex+= lineCount;
//                            break;
//                        case EXTENDS_COUNT:
//                            extendsCount++;
//                            extendsIndex+= lineCount;
//                            break;
//                        case THROWS_COUNT:
//                            throwsCount++;
//                            throwsIndex+= lineCount;
//                            break;
//                        case IMPLEMENTS_COUNT:
//                            implementsCount++;
//                            implementsIndex+= lineCount;
//                            break;
//                        case THIS_COUNT:
//                            thisCount++;
//                            thisIndex+= lineCount;
//                            break;
//                        case FINAL_COUNT:
//                            finalCount++;
//                            finalIndex+= lineCount;
//                            break;
//                    }
//                }
//                lineCount++;
//            }
//            sc.close();
//            System.out.println(
//                    "int\n"+intCount +Arrays.toString(intIndex)+"double\n"+ doubleCount +Arrays.toString(doubleIndex)
//                            +"char\n"+ charCount +Arrays.toString(charIndex) +
//                            "float\n"+ floatCount +Arrays.toString(floatIndex)+
//                            "bit\n" + bitCount +Arrays.toString(bitIndex) +
//                            "boolean\n"+booleanCount +Arrays.toString(booleanIndex) +
//                            "byte\n"+byteCount +Arrays.toString(byteIndex)+
//                            "short\n"+shortCount +Arrays.toString(shortIndex) +
//                            "String\n"+ stringCount+Arrays.toString(stringIndex) +
//                            "if\n"+ ifCount +Arrays.toString(ifIndex)+
//                            "else\n"+elseCount +Arrays.toString(elseIndex)+
//                            "switch\n"+switchCount+Arrays.toString(switchIndex) +
//                            "for\n"+ forCount+Arrays.toString(forIndex) +
//                            "while\n"+whileCount+Arrays.toString(whileIndex) +
//                            "do\n"+ doCount +Arrays.toString(doIndex)+
//                            "try\n"+ tryCount+Arrays.toString(tryIndex) +
//                            "catch\n"+ catchCount+Arrays.toString(catchIndex) +
//                            "public\n"+ publicCount+Arrays.toString(publicIndex) +
//                            "static\n"+staticCount+Arrays.toString(staticIndex) +
//                            "void\n" + voidCount+Arrays.toString(voidIndex) +
//                            "main\n"+mainCount+Arrays.toString(mainIndex) +
//                            "Private\n"+ privateCount+Arrays.toString(privateIndex) +
//                            "abstract\n"+ abstractCount+Arrays.toString(abstractIndex) +
//                            "extends\n"+ extendsCount +Arrays.toString(extendsIndex) +
//                            "throws\n"+ throwsCount+Arrays.toString(throwsIndex) +
//                            "implenents\n"+implementsCount +Arrays.toString(implementsIndex)+
//                            "this\n"+thisCount +Arrays.toString(thisIndex)+
//                            "final\n"+ finalCount+Arrays.toString(finalIndex)
//            );
//        } catch (IOException e) {
//            System.out.println("Something went wrong: ");
//            e.printStackTrace();
//        }
////        System.out.println("Read and output text file line-by-line");
////
////        try {
////            Scanner sc = new Scanner(new File(file));
////
////            while (sc.hasNext()) {
////                String str = sc.nextLine();
////                System.out.println(str);
////            }
////            sc.close();
////        } catch (IOException e) {
////        }
//    }
//    private enum Identifiers
//    {
//        INT("int"), DOUBLE_COUNT("double"), CHAR_COUNT("char"), FLOAT_COUNT("Float"), BIT_COUNT("bit"),
//        BOOLEAN_COUNT("boolean"), BYTE_COUNT("byte"), SHORT_COUNT("short"), STRING_COUNT("String"), IF_COUNT("if"),
//        ELSE_COUNT("else"), SWITCH_COUNT("switch"), FOR_COUNT("for"), WHILE_COUNT("while"), DO_COUNT("do"),
//        TRY_COUNT("try"), CATCH_COUNT("catch"), PUBLIC_COUNT("public"), STATIC_COUNT("static") , VOID_COUNT("void")
//        , MAIN_COUNT("main"), PRIVATE_COUNT("private"), ABSTRACT_COUNT("abstract"), EXTENDS_COUNT("extends"),
//        THROWS_COUNT("throws"), IMPLEMENTS_COUNT("implements"), THIS_COUNT("this"), FINAL_COUNT("final");
//        Identifiers(String s) {
//
//        }
//
//    }
//    private static boolean enumContainsValue(String identifier)
//    {
//        for (Identifiers id : Identifiers.values())
//        {
//            if (id.name().equals(identifier))
//            {
//                return true;
//            }
//        }
//
//        return false;
//    }
//}
