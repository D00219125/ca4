package Ca4;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Aidan Bligh D00219125
 */
public class Q3Main {

    /**
     *Asks user to enter id to be scanned
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("These are the employee's id's, cant use getters like in the 1st main so have to rely on copy and paste.");
        EmployeeHashMap emp = new EmployeeHashMap();
        Scanner kb = new Scanner(System.in);
        int quit = 0;
        String id;
        try
        {
            while(quit != -1)
            {
                System.out.println("Enter the id you would like to search for");
                id = kb.nextLine();
                idScanner(id,emp.getHashMap());
                System.out.println("press enter to continue");
                kb.nextLine();
                System.out.println("Enter -1 to quit or any other number to continue");
                quit = Integer.parseInt(kb.nextLine());

            }
        }
        catch(InputMismatchException e)
        {
            System.out.println("You entered a string instead of a number. That's bad.");
        }
        catch(NumberFormatException e)
        {
            System.out.println("looks like you smacked the keyboard without thinking");
        }
    }

    /**
     *takes in id String and prints it's employees values if the id is stored as a key in the hashset.
     * @param id
     * @param idMap
     */
    public static void idScanner(String id, HashMap<String,Employee> idMap)
    {
        if(!idMap.containsKey(id) || id.equals(null))
        {
            System.out.println("There is no such id in the system try again");
        }
        if(idMap.containsKey(id))
        {
            System.out.println(idMap.get(id).toString());
        }
    }
}
