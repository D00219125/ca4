package Ca4;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import Ca4.Enums.Departments;

/**
 *
 * @author Aidan Bligh D00219125
 */
public final class Employee {
    /*On their display, security personnel need to be able to see
    an employee’s ID, name, photo, department, and a list of the floors the employee can
    access. (We will simply represent the photo as a String e.g. “Photo of Tom�?).
    Sample employee ID: “STF2020�?. Identify and implement this system using appropriate data
    structures, Collections, and methods. Discuss these with your lecturer before implementation.

    Implement Question 1 (Employee security system) by making the Collection entries
    Immutable (Immutable Employee), in which case no encapsulation of the collection entries
    is required.
    This will be marked using a grading system. You must be able to clearly demonstrate your
    understanding of your solution.
    Note: only needed to change the class to final as 1) there are no subclasses 2) there were no setter methods,
    adding final made it immutable*/
    private String id;
    private String name;
    private String photo;
    private Departments department;
    private List<Floors> floors;

    Departments dep;

    /**
     *constructor for employee class
     * @param name
     * @param photo
     * @param department
     */
    public Employee( String name, String photo, Departments department) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.photo = photo;
        this.department = department;
        this.floors = assignFloors(department);
    }

    
    private enum Floors
    {
        BASEMENT, GROUND_FLOOR, CAFETERIA, SALES, NETWORKS
    }
    private List<Floors> assignFloors(Departments department) {
        List<Floors> availableFloors = new ArrayList();
        switch(department)
        {
            case CUSTODIAN:
                availableFloors.add(Floors.BASEMENT);
                        availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        availableFloors.add(Floors.SALES);
                        availableFloors.add(Floors.NETWORKS);
                        break;
            case COOK:  availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        availableFloors.add(Floors.BASEMENT);
                        break;
            case MANAGER:availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        availableFloors.add(Floors.SALES);
                        availableFloors.add(Floors.NETWORKS);
                        break;
            case SALESMAN: availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        availableFloors.add(Floors.SALES);
                        break;
            case NETWORK_ADMIN:availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        availableFloors.add(Floors.NETWORKS);
                        break;
            case RECEPTIONIST:availableFloors.add(Floors.GROUND_FLOOR);
                        availableFloors.add(Floors.CAFETERIA);
                        break;
        }
        return availableFloors;
    }

    //use getters for card reader

    /**
     *
     * @return employees id
     */

    public String getId() {
        return id;
    }

    /**
     *
     * @return employees name
     */
    public String getName() {
        return name;
    }




    @Override
    public String toString() {
        return "Employee{" +
                "Id='" + id + '\'' +
                ", Name='" + name + '\'' +
                ", Photo='" + photo + '\'' +
                ", Department=" + department +
                ", Floors=" + floors +
                '}';
    }
}
