package Ca4;

import Ca4.Enums.Departments;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Aidan Bligh D00219125
 */
public class Main {
    /*
    q1 and q3 are seperate.
    * Question 1 [30 marks]
    An office security system is required to identify employees based on their staff cards.
    Employees swipe their staff card at a reader, and the reader scans the card and returns the
    employee ID, which is fed into the software system. Employee information must be
    accessed and immediately displayed for verification by the person at the security desk.
    Employees have access to different floors of the building depending on their security
    clearance. (Floors: -1, 0, 1, 2, 3). On their display, security personnel need to be able to see
    an employee’s ID, name, photo, department, and a list of the floors the employee can
    access. (We will simply represent the photo as a String e.g. “Photo of Tom�?).
    Sample employee ID: “STF2020�?. Identify and implement this system using appropriate data
    structures, Collections, and methods. Discuss these with your lecturer before implementation.
*/

    /**
     *Makes 7 employees and adds them to a hashmap and calls the idScanner()
     * @param args
     */


    public static void main(String[] args) {
        HashMap<String, Employee> idMap = new HashMap<String, Employee>();
        //question 1
        Employee test = new Employee("Harvey", "Photo of Harvey", Departments.MANAGER);
        idMap.put(test.getId(),test);
        Employee test2 = new Employee("Joey", "Photo of Joey", Departments.COOK);
        idMap.put(test2.getId(),test2);
        Employee test3 = new Employee("Mick", "Photo of Mick", Departments.NETWORK_ADMIN);
        idMap.put(test3.getId(),test3);
        Employee test4 = new Employee("Mike", "Photo of Mike", Departments.RECEPTIONIST);
        idMap.put(test4.getId(),test4);
        Employee test5 = new Employee("Marv", "Photo of Marv", Departments.CUSTODIAN);
        idMap.put(test5.getId(),test5);
        Employee test6 = new Employee("Mark", "Photo of Mark", Departments.SALESMAN);
        idMap.put(test6.getId(),test6);
        Employee test7 = new Employee("Monty", "Photo of Monty", Departments.SALESMAN);
        idMap.put(test7.getId(),test7);

        String id = test.getId();
        System.out.println("Id entered = "+id);

        idScanner(test.getId(),idMap);

        String rand = "46900122-9d85-4ea6-b317-4e73fddff1f2";
        System.out.println("random id entered = " + rand);
        idScanner(rand,idMap);
    }

    /**
     *takes in id String and prints it's employees values if the id is stored as a key in the hashset.
     * @param id
     * @param idMap
     */
    public static void idScanner(String id, HashMap<String,Employee> idMap)
    {
        if(!idMap.containsKey(id) || id.equals(null))
        {
            System.out.println("There is no such id in the system try again");
        }
        if(idMap.containsKey(id))
        {
            System.out.println(idMap.get(id).toString());
        }
    }
}